"use client";
import { Projects } from "@/app/ProjectsPage/Projects";
import Image from "next/image";
import Link from "next/link";
import React from "react";
import parse from "html-react-parser";

const ProjectGrid = () => {
    return (
        <div className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-10 sm:gap-24 m-10 sm:m-20">
            {Projects.map((ele) => (
                <div
                    key={ele.id}
                    className="bg-white border-2 border-black rounded-xl overflow-hidden transition-transform duration-300 hover:scale-105 flex flex-col"
                >
                    <div className="relative">
                        <Image
                            className="w-full h-48 object-cover"
                            src={ele.display_image}
                            alt={ele.project_title}
                        />
                        <div className="absolute inset-0 bg-gradient-to-t from-blue-800 to-transparent"></div>
                    </div>
                    <div className="p-4 flex-grow">
                        <h2 className="text-lg font-bold mb-2 text-blue-800">
                            {ele.project_title}
                        </h2>
                        <p className="text-gray-600 mb-4 line-clamp-2">
                            {parse(ele.display_desc ? ele.display_desc : ele.desc1)}
                        </p>
                    </div>
                    <div className="p-4  rounded-b-xl">
                    <Link
                            href={`/ProjectsPage/${ele.id.toString()}`}
                            className="inline-block bg-blue-800 hover:bg-blue-900 text-white font-bold py-2 px-4 rounded"
                        >
                            Know More
                        </Link>
                    </div>
                </div>
            ))}
        </div>
    );
};

export default ProjectGrid;