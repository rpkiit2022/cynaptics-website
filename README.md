
Cynaptics Website: A Comprehensive Guide
========================================

Welcome to the Cynaptics website! This README file is designed to help you navigate and manage the website effortlessly, even if you're a layman. We've styled the content and added the names of our valuable contributors to make it more engaging and informative.

Copy code

Contributors
------------

We would like to express our gratitude to the following individuals for their invaluable contributions to this project:

*   Pradeep Kumar Rebbavarapu
*   Ayush Awasti
*   Abhinav Gangil
*   Vaidehi Bhat
*   Varad

Handling the Website
--------------------

### 1\. File Structure

*   **`public/Models`**: This directory contains the Blender models used on the website.
*   **`public/Images`**: Here, you'll find all the images used throughout the website.
*   **`public/Videos`**: This directory houses the video files featured on the website.
*   **`src/app`**: All the website pages are located in this folder.
*   **`src/components`**: This directory contains reusable components used across the website.
*   **`src/Types`**: TypeScript type definitions are stored in this folder.

### 2\. Changing the Projects Page Data

1.  Navigate to `src/app/ProjectsPage/Projects`.
2.  You will find an array of project objects.
3.  To add a new project, create a new object with a unique ID.
4.  Fill in the remaining details with appropriate values or sentences.
5.  To upload a project image, go to `public/images/Projects` and add your image file.
6.  **Important Note**: In `src/app/ProjectsPage/Projects`, you need to specify the project image path correctly. Follow this format:
    
        project_image: require("relative/path/to/your/ProjectImage.jpg");
    
    For example:
    
        project_image: require("../../../public/images/Projects/ProjectImage.jpg");
    
7.  Once completed, your new project will appear on the website.

### 3\. Changing the Events Page Data

1.  Navigate to `src/app/EventsPage/Events`.
2.  You will find an array of event objects.
3.  To add a new event, create a new object with a unique ID.
4.  Fill in the remaining details with appropriate values or sentences.
5.  To upload an event image, go to `public/images/Events` and add your image file.
6.  **Important Note**: In `src/app/EventsPage/Events`, you need to specify the event image path correctly. Follow this format:
    
        img: require("relative/path/to/your/EventImage.jpg");
    
    For example:
    
        img: require("../../../public/images/Events/EventImage.jpg");
    
7.  Once completed, your new event will appear on the website.

### 4\. Changing the Our Team Page Data

1.  Navigate to `src/app/OurTeamPage/TeamMembers`.
2.  You will find an array of team member objects.
3.  To add a new team member, create a new object with a unique ID.
4.  Fill in the remaining details with appropriate values or sentences.
5.  To upload a team member's image, go to `public/images/Members` and add your image file.
6.  **Important Note**: In `src/app/OurTeamPage/TeamMembers`, you need to specify the team member's image path correctly. Follow this format:
    
        image: require("relative/path/to/your/MemberImage.jpg");
    
    For example:
    
        image: require("../../../public/images/Members/MemberImage.jpg");
    
7.  Once completed, your new team member will appear on the website.

### 5\. Changing the Video Description

1.  Navigate to `src/components/VideoPage`.
2.  Locate the div tags with specific IDs describing the video content.
3.  For example, the div with `id="description of quick draw video"` contains the description for the Quick Draw video.
4.  Modify the content within these div tags to update the respective video descriptions.

### 6\. Changing the About Us Page Description

1.  Navigate to `src/app/AboutUsPage`.
2.  You can modify the About Us page content in this file.

**Note**: During production, ensure you review the `.env` file. There is a variable called `SITE_URL` that holds the production site URL. It is crucial for generating the website's sitemaps correctly.

We hope this guide has provided you with a clear understanding of managing the Cynaptics website. If you have any further questions or need additional assistance, please don't hesitate to reach out to our team.